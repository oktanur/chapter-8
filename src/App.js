import { Routes, Route } from "react-router-dom";
import LoginPage from "./pages/login/login_page";
import RegisterPage from "./pages/register/register_page";
import GameHistory from "./pages/game_history/game_history";
import CreateRoom from "./pages/crate_room/create_room";
import VsPlayer from "./pages/vs_player/vs_player";
import VsCom from "./pages/vs_com/vs_com";
import RoomPage from "./pages/room/room_page";
import ErrorPage from "./pages/errorpage/errorpage";
function App() {
  return (
    <Routes>
      <Route path="/login" element={<LoginPage />} />
      <Route path="/register" element={<RegisterPage />} />
      <Route path="/dashboard" element={<RoomPage />} />
      <Route path="/game-history" element={<GameHistory />} />
      <Route path="/create-room" element={<CreateRoom />} />
      <Route path="/against-player" element={<VsPlayer />} />
      <Route path="/against-com" element={<VsCom />} />
      <Route path="*" element={<ErrorPage />} />
    </Routes>
  );
}

export default App;
