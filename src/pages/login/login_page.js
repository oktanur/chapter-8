import Login from "../../components/organisms/login/login";
import "./login_page.css";
import { GameImage } from "../../components/atoms/image/image";
const LoginPage = () => {
  return (
    <div className="container1">
      <div className="innerContainer1">
        <GameImage />
      </div>
      <Login />
    </div>
  );
};
export default LoginPage;
