import Register from "../../components/organisms/register/register";
import "./register_page.css";
import { GameImage } from "../../components/atoms/image/image";
const RegisterPage = () => {
  return (
    <div className="container2">
      <div className="innerContainer2">
        <GameImage />
      </div>
      <Register />
    </div>
  );
};
export default RegisterPage;
