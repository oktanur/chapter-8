import Rooms from "../../components/organisms/rooms/rooms";
import GameHistory from "../../components/organisms/game_history/game_history";
import "./room_page.css";
const RoomPage = () => {
  return (
    <div className="mainContainer">
      <Rooms />
      <GameHistory
        fullname={"Mehdi Sungkara"}
        gender={"male"}
        address={"Jl. Cempaka 11, Jakarta Pusat"}
        phoneNumber={"+62856784844"}
        dateOfBirth={"1998-10-29"}
      />
    </div>
  );
};
export default RoomPage;
