import { RockPicture } from "../../components/atoms/image/image";
import { ScissorsPicture } from "../../components/atoms/image/image";
import { PapperPicture } from "../../components/atoms/image/image";
import Button from "../../components/atoms/button/button";
import "./vs_com.css";
function VsCom() {
  return (
    <div className="containerPlayer">
      <div className="innerPlayerCont">
        <div className="player">
          <div className="player1">
            <h3>Player 1</h3>
          </div>
          <div>
            <RockPicture />
          </div>
          <div>
            <PapperPicture />
          </div>
          <div>
            <ScissorsPicture />
          </div>
        </div>
        <div className="vs">VS</div>
        <div className="player">
          <div>
            <h3>COM</h3>
          </div>
          <div>
            <RockPicture />
          </div>
          <div>
            <PapperPicture />
          </div>
          <div>
            <ScissorsPicture />
          </div>
        </div>
      </div>
      <Button name="Play" />
    </div>
  );
}

export default VsCom;
