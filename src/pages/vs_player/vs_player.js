import { RockPicture } from "../../components/atoms/image/image";
import { ScissorsPicture } from "../../components/atoms/image/image";
import { PapperPicture } from "../../components/atoms/image/image";
import "./vs_player.css";
function VsPlayer() {
  return (
    <div className="containerPlayer">
      <h3>Room C</h3>
      <div className="innerPlayerCont">
        <div className="player">
          <div className="player1">
            <h3>Player 1</h3>
          </div>
          <div>
            <RockPicture />
          </div>
          <div>
            <PapperPicture />
          </div>
          <div>
            <ScissorsPicture />
          </div>
        </div>
        <div className="vs">VS</div>
        <div className="player">
          <div>
            <h3>Player 2</h3>
          </div>
          <div>
            <RockPicture />
          </div>
          <div>
            <PapperPicture />
          </div>
          <div>
            <ScissorsPicture />
          </div>
        </div>
      </div>
    </div>
  );
}

export default VsPlayer;
