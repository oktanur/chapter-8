import React, { useState } from "react";
import "./game_history.css";
import TableHistory from "../../components/molecules/table_history/table_history";
function GameHistory() {
  const [data, setData] = useState([
    {
      id: 1,
      roomName: "BigBoss",
      time: "Mei 20th 07.00",
      result: "WON",
    },
    {
      id: 2,
      roomName: "BigBoss",
      time: "Mei 20th 07.00",
      result: "WON",
    },
    {
      id: 3,
      roomName: "BigBoss",
      time: "Mei 20th 07.00",
      result: "WON",
    },
    {
      id: 4,
      roomName: "BigBoss",
      time: "Mei 20th 07.00",
      result: "WON",
    },
    {
      id: 5,
      roomName: "BigBoss",
      time: "Mei 20th 07.00",
      result: "WON",
    },
    {
      id: 6,
      roomName: "BigBoss",
      time: "Mei 20th 07.00",
      result: "WON",
    },
  ]);
  return (
    <div className="gameHisContainer">
      <div className="gameHisTitle">
        <h3>Game History</h3>
      </div>
      <div>
        <table className="zigzag">
          <thead>
            <tr>
              <th class="header">Room Name</th>
              <th class="header">Date</th>
              <th class="header">Result</th>
            </tr>
          </thead>
          <tbody>
            {data.map((i) => {
              return (
                <TableHistory
                  key={i.id}
                  roomName={i.roomName}
                  time={i.time}
                  result={i.result}
                />
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default GameHistory;
