import InputForm from "../../components/molecules/input/input_form";
import { RockPicture } from "../../components/atoms/image/image";
import { PapperPicture } from "../../components/atoms/image/image";
import { ScissorsPicture } from "../../components/atoms/image/image";
import React, { useState } from "react";
import Button from "../../components/atoms/button/button";
import "./create_room.css";
function CreateRoom() {
  const [choice, setChoice] = useState("");
  console.log(choice);
  return (
    <div className="myRoom">
      <div className="inputRoom">
        <p className="inputLabel">Please input room name</p>
        <InputForm id={"1"} htmlFor={"roomName"} type={"text"} />
      </div>
      <p className="playerChoice">Please pick your choice</p>
      <div className="choice">
        <div
          onClick={() => {
            if (choice === "") setChoice("rock");
          }}
          style={{
            backgroundColor:
              choice === "rock" ? "rgba(255, 181, 72, 1)" : "transparent",
          }}
        >
          <RockPicture />
        </div>
        <div
          onClick={() => {
            if (choice === "") setChoice("papper");
          }}
          style={{
            backgroundColor:
              choice === "papper" ? "rgba(255, 181, 72, 1)" : "transparent",
          }}
        >
          <PapperPicture />
        </div>
        <div
          onClick={() => {
            if (choice === "") setChoice("scissors");
          }}
          style={{
            backgroundColor:
              choice === "scissors" ? "rgba(255, 181, 72, 1)" : "transparent",
          }}
        >
          <ScissorsPicture />
        </div>
      </div>
      <Button name="Save" />
    </div>
  );
}

export default CreateRoom;
