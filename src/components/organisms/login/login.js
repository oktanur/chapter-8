import InputForm from "../../molecules/input/input_form";
import { H3 } from "../../atoms/text/text";
import Button from "../../atoms/button/button";
import "./login.css";
const items = [
  {
    id: 1,
    htmlFor: "email",
    label: "Email",
    type: "email",
  },
  {
    id: 2,
    htmlFor: "password",
    label: "Password",
    type: "password",
  },
];
const Login = () => {
  return (
    <div className="loginBox">
      <div className="titleContainer">
        <H3 text="Log In" />
      </div>
      <form method="post">
        {items.map((i) => (
          <InputForm
            key={i.id}
            htmlFor={i.htmlFor}
            label={i.label}
            type={i.type}
          />
        ))}
        <div className="buttonContainer">
          <Button type="submit" name="Login" />
        </div>
      </form>
    </div>
  );
};
export default Login;
