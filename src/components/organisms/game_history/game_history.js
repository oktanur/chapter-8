import Bio from "../../molecules/bio/bio";
import { Link } from "react-router-dom";
import { ImageProfile } from "../../atoms/profile/profile";
import "./game_history.css";
const GameHistory = ({
  fullname,
  gender,
  address,
  phoneNumber,
  dateOfBirth,
}) => {
  return (
    <div className="container4">
      <ImageProfile />
      <Bio
        fullname={fullname}
        gender={gender}
        address={address}
        phoneNumber={phoneNumber}
        dateOfBirth={dateOfBirth}
      />
      <Link to={"/game-history"}>
        <div>Game History</div>
      </Link>
    </div>
  );
};
export default GameHistory;
