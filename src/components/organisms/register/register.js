import InputForm from "../../molecules/input/input_form";
import { H3 } from "../../atoms/text/text";
import Button from "../../atoms/button/button";
import "./register.css";
const items = [
  {
    id: 1,
    htmlFor: "username",
    label: "Username",
    type: "text",
  },
  {
    id: 2,
    htmlFor: "email",
    label: "Email",
    type: "email",
  },
  {
    id: 3,
    htmlFor: "password",
    label: "Password",
    type: "password",
  },
];
const Register = () => {
  return (
    <div className="registerBox">
      <div className="titleContainer">
        <H3 text="Sign Up" />
      </div>
      <form method="post">
        {items.map((i) => (
          <InputForm
            key={i.id}
            htmlFor={i.htmlFor}
            label={i.label}
            type={i.type}
          />
        ))}
        <div className="buttonContainer">
          <Button type="submit" name="Sign Up" />
        </div>
      </form>
    </div>
  );
};
export default Register;
