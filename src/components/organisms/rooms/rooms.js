import React, { useState } from "react";
import { Link } from "react-router-dom";
import Room from "../../molecules/room/room";
import "./rooms.css";
const Rooms = () => {
  const [items, setItems] = useState([
    {
      id: 1,
      roomName: "iniRoomku",
      winner: "BocahKicik",
    },
    {
      id: 2,
      roomName: "simalakama",
      winner: "nurwidyas",
    },
    {
      id: 3,
      roomName: "simalakama",
      winner: "nurwidyas",
    },
    {
      id: 4,
      roomName: "simalakama",
      winner: "nurwidyas",
    },
    {
      id: 5,
      roomName: "simalakama",
      winner: "nurwidyas",
    },
  ]);
  return (
    <div className="container">
      <div className="vsCom">
        <Link to={"/against-com"}>
          <h3>PLAY VS COM</h3>
        </Link>
      </div>
      <div className="innerContainer">
        <div className="vsCom">
          <Link to={"/create-room"}>
            <h4>Create New Room</h4>
          </Link>
        </div>
        <div className="roomContainer">
          {items.map((i) => (
            <Room key={i.id} roomName={i.roomName} winner={i.winner} />
          ))}
        </div>
      </div>
    </div>
  );
};
export default Rooms;
