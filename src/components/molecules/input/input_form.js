import Input from "../../atoms/input/input";
import Label from "../../atoms/label/label";
import "./input_form.css";

const InputForm = ({ id, htmlFor, label, type }) => {
  return (
    <div className="userBox">
      <Label key={id} htmlFor={htmlFor} label={label} />
      <Input name={htmlFor} type={type} placeholder={label} />
    </div>
  );
};
export default InputForm;
