function TableHistory({ roomName, time, result }) {
  return (
    <tr className="tableHis">
      <td>{roomName}</td>
      <td>{time}</td>
      <td>{result}</td>
    </tr>
  );
}

export default TableHistory;
