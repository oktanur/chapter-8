import "./room.css";
const Room = ({ roomName, winner }) => {
  return (
    <div className="singleRoom">
      <h4>{roomName}</h4>
      <p>winner: {winner}</p>
    </div>
  );
};
export default Room;
