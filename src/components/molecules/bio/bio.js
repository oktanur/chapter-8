import "./bio.css";
const Bio = ({ fullname, gender, address, phoneNumber, dateOfBirth }) => {
  return (
    <div className="singleRoom">
      <p>Fullname: {fullname}</p>
      <p>Gender: {gender}</p>
      <p>Address: {address}</p>
      <p>Phone Number: {phoneNumber}</p>
      <p>Date of Birth: {dateOfBirth}</p>
    </div>
  );
};
export default Bio;
