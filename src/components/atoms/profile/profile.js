import profile from "../../assets/midas-profile.jpg";
import "./profile.css";
export const ImageProfile = () => {
  return <img src={profile} className="profile" alt="midas profile" />;
};
