import "./text.css";
export const H3 = (props) => {
  return <h3>{props.text}</h3>;
};
export const H4 = (props) => {
  return <h4>{props.textHeading4}</h4>;
};
export const Paragraph = (props) => {
  return <p>{props.textParagraph}</p>;
};
