import "./image.css";
import Image from "../../assets/game.jpg";
import Rock from "../../assets/batu.png";
import Papper from "../../assets/kertas.png";
import Scissors from "../../assets/gunting.png";
export const GameImage = () => {
  return <img src={Image} alt="game" />;
};
export const RockPicture = () => {
  return <img src={Rock} alt="rock" className="rock" />;
};
export const PapperPicture = () => {
  return <img src={Papper} alt="papper" className="papper" />;
};
export const ScissorsPicture = () => {
  return <img src={Scissors} alt="scissors" className="scissors" />;
};
